Personal site for visual designer Luis David Goldberg Hemo.\
[http://www.ldgh.com.ar](http://www.ldgh.com.ar)

# Local environment

## Pre-requisites
* npm
* npm live-server package

To install `live-server` globally, run the following command:
```
npm install -g live-server
```

## Assets
Compile assets:
```
npm run dev
```

Watch asset file changes:
```
npm run watch
```

## Run server
The following command will start an http server at `http://localhost:8081`:
```
npm run serve
```