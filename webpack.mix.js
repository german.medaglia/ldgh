const mix = require('laravel-mix');

mix
    .styles([
        './node_modules/bootstrap/dist/css/bootstrap.min.css'
    ], 'public/css/lib.css')
    .js('src/app.js', 'public/js');

