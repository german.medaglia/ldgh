import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home.vue'
import Illustrations from './components/Illustrations.vue'
import Bio from './components/Bio.vue'
import Contact from './components/Contact.vue'
import Project from './components/Project.vue'
import NotFound from './components/NotFound.vue'

Vue.use(Router)

const routes = [
    { path: '/', name: 'home', component: Home },
    { path: '/illustrations', name: 'illustrations', component: Illustrations },
    { path: '/bio', name: 'bio', component: Bio },
    { path: '/contact', name: 'contact', component: Contact },
    { path: '/design-and-animation/:projectCode', name: 'project', component: Project },
    { path: '*', name: 'not-found', component: NotFound }
]

export default new Router({
    routes, // short for `routes: routes`
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})
