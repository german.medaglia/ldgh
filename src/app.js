import Vue from 'vue'
import router from './router'

Vue.component('nav-desktop', require('./components/NavDesktop.vue').default);
Vue.component('nav-mobile', require('./components/NavMobile.vue').default);

const app = new Vue({
    router
}).$mount('#app');


